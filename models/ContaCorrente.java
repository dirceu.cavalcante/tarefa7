package models;

public class ContaCorrente extends Conta{
    public double chequeEspecial;

    public ContaCorrente(int numero, int agencia, String banco, String nome, long cpf, 
    double saldo, double chequeEspecial){
        super(numero, agencia, banco, nome, cpf, saldo);
        this.chequeEspecial = chequeEspecial;
    }

    public double getSaldo(){
        return saldo + chequeEspecial;
    }

    public void setSaldo(double saldo){
        this.saldo = saldo;
    }

    public void deposito(double deposito){
        System.out.println("Depositando R$ " + deposito + " na sua conta...");
        setSaldo((saldo + deposito));
        System.out.println("Saldo Atual R$ " + getSaldo());
    }

    public void saque (double saque){
        double transacao = getSaldo() - saque;
        if (transacao < 0.0)
        {
            System.out.println("\nSaldo Insuficiente!!!");
        } else{
            System.out.println("Sacando R$ "+ saque + " ...");
            System.out.println("Saque Concluído!");
            setSaldo((saldo - saque));
            System.out.println("Saldo Atual: " + getSaldo());
            
        }
    }
    
    public String toString(){
        return "Conta Corrente = {Agência " + getAgencia() +
        ", Número " + getNumero() + 
        ", " + getBanco() +
        ", " + getNome() +
        ", CPF " + getCPF() + 
        ", Saldo R$ " + (getSaldo()+chequeEspecial) + "}";
    }
}
