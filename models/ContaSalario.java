package models;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ContaSalario extends Conta{
    private static int inicioMes = 1;
    public int quantidadeSaldo = 3;
    private boolean fezAjuste = false;
    public Date hoje = Calendar.getInstance().getTime();
    public SimpleDateFormat sdf = new SimpleDateFormat("dd");
   
    public void ajusteQuantidadeSaldo(){
        if (((inicioMes <= Integer.parseInt(sdf.format(hoje))) && (!fezAjuste))){
            this.quantidadeSaldo = 3;
            fezAjuste = true;            
        }
        quantidadeSaldo--;
    }

    public ContaSalario(int numero, int agencia, String banco, String nome, long cpf, 
    double saldo){
        super(numero, agencia, banco, nome, cpf, saldo);
    }

        
    public double getSaldo(){
        if (quantidadeSaldo > 0){
            ajusteQuantidadeSaldo();
            System.out.println("\n LIMITE DE CONSULTAS RESTANTES: " + quantidadeSaldo);
            return saldo;
        }
        else{
            System.out.println("\nOPERAÇÃO INVÁLIDA! ATINGIU O LIMITE DE CONSULTAS MENSAL!");
            return 0;
        }
    }
    public double getSaldoSistema(){
        return saldo;
    }
    

    public void setSaldo (double saldo){
        this.saldo = saldo;
    }

    public void deposito(double deposito){
        setSaldo(arredonda(getSaldoSistema() + deposito));
        System.out.println("\n Depositando R$ " + deposito);
        System.out.println("Saldo atual R$ "+ getSaldoSistema());
    }

    public void saque(double saque){
        double transacao = getSaldoSistema() - saque;
        if (transacao < 0.0)
        {
            System.out.println("\nSaldo Insuficiente!!!");
        } else{
            System.out.println("Sacando R$ "+ saque + " ...");
            System.out.println("Saque Concluído!");
            setSaldo(arredonda(saldo - saque));
            System.out.println("Saldo Atual: " + getSaldoSistema());
            
        }
    }
    
    public String toString(){
        return "Conta Corrente = {Agência " + getAgencia() +
        ", Número " + getNumero() + 
        ", " + getBanco() +
        ", " + getNome() +
        ", CPF " + getCPF() + 
        ", Quantidade de Saldos Mensal = " + quantidadeSaldo + "}";
    }

    public double arredonda(double valor){
        double d = Math.round(valor*100);
        return d/100;
    }
}
