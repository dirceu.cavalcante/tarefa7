import java.util.Scanner;

import models.*;

public class Main {

    static ContaCorrente cc1 = new ContaCorrente(1,1,"BB","João Medeiros Filho",33344455566L,100.00,1000.00);
    static ContaPoupanca cp1 = new ContaPoupanca(2, 1, "BB", "João Medeiros Filho", 33344455566L, 100.00, 1.00,30);
    static ContaSalario cs1 = new ContaSalario(2, 1, "BB", "João Medeiros Filho",25812345679L, 1000.00);
    
    public static void main (String[] args){
        int opcao;
        do{
            System.out.println("\n ******************************");
            System.out.println("\n ********MENU PRINCIPAL********");
            System.out.println("\n ******************************");
            System.out.println("1 - Transções com Conta Corrente");
            System.out.println("2 - Transções com Conta Poupança");
            System.out.println("3 - Transções com Conta Salário");
            System.out.println("0 - Sair");
            System.out.print("Digite a opção desejada: ");

            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();
            menuPrincipal(opcao);   
        }while (opcao != 0);
    }    
    public static void menuPrincipal(int opcao){
        switch (opcao) {
            case 1:{
                menuContaCorrente();
                break;
            }
            case 2:{
                menuContaPoupanca();
                break;
            }
            case 3:{
                menuContaSalario();
                break;
            }
        }
    }
    public static void menuContaCorrente(){

            int opcao;
            do{
            System.out.println("\n ******************************");
            System.out.println("********CONTA CORRENTE********");
            System.out.println("******************************");
            System.out.println("1 - Consultar Saldo");
            System.out.println("2 - Realizar Depósito");
            System.out.println("3 - Efetuar Saque");
            System.out.println("0 - Sair");
            System.out.print("Digite a opção desejada: ");

            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();

            switch (opcao) {
                case 1:{
                    System.out.println("SALDO: R$ "+cc1.getSaldo());
                    break;
                }
                case 2:{
                   System.out.print("Digite o valor a ser depositado: R$ ");
                   double deposito = scanner.nextDouble();
                   cc1.deposito(deposito);
                   System.out.print("Operação Finalizada! ");
                   System.out.println("SALDO: R$ "+cc1.getSaldo());
                   break;
                }
                case 3:{
                   System.out.print("Digite o valor a ser sacado: R$ ");
                   double saque = scanner.nextDouble();
                   cc1.saque(saque);
                   System.out.println("Operação Finalizada! ");
                   System.out.println("SALDO: R$ "+cc1.getSaldo());
                    break;
                }
            }
        }while (opcao!=0);
    
        
    
    }
    public static void menuContaPoupanca(){

        int opcao;
        do{
        System.out.println("\n ******************************");
        System.out.println("********CONTA POUPANÇA********");
        System.out.println("******************************");
        System.out.println("1 - Consultar Saldo");
        System.out.println("2 - Realizar Depósito");
        System.out.println("3 - Efetuar Saque");
        System.out.println("0 - Sair");
        System.out.print("Digite a opção desejada: ");

        Scanner scanner = new Scanner(System.in);
        opcao = scanner.nextInt();

        switch (opcao) {
            case 1:{
                System.out.println("SALDO: R$ "+cp1.getSaldo());
                break;
            }
            case 2:{
               System.out.print("Digite o valor a ser depositado: R$ ");
               double deposito = scanner.nextDouble();
               cp1.deposito(deposito);
               System.out.print("Operação Finalizada! ");
               System.out.println("SALDO: R$ "+cp1.getSaldo());
               break;
            }
            case 3:{
               System.out.print("Digite o valor a ser sacado: R$ ");
               double saque = scanner.nextDouble();
               cp1.saque(saque);
               System.out.println("Operação Finalizada! ");
               System.out.println("SALDO: R$ "+cp1.getSaldo());
                break;
            }
        }
    }while (opcao!=0);
    }
    public static void menuContaSalario(){

        int opcao;
        do{
        System.out.println("\n******************************");
        System.out.println("********CONTA  SALÁRIO********");
        System.out.println("******************************");
        System.out.println("1 - Consultar Saldo (Consultas disponíveis: "+cs1.quantidadeSaldo+")");
        System.out.println("2 - Realizar Depósito");
        System.out.println("3 - Efetuar Saque");
        System.out.println("0 - Sair");
        System.out.print("Digite a opção desejada: ");

        Scanner scanner = new Scanner(System.in);
        opcao = scanner.nextInt();

        switch (opcao) {
            case 1:{
                System.out.println("SALDO: R$ "+cs1.getSaldo());
                break;
            }
            case 2:{
               System.out.print("Digite o valor a ser depositado: R$ ");
               double deposito = scanner.nextDouble();
               cs1.deposito(deposito);
               System.out.print("Operação Finalizada! ");
               System.out.println("SALDO: R$ "+cs1.getSaldoSistema());
               break;
            }
            case 3:{
               System.out.print("Digite o valor a ser sacado: R$ ");
               double saque = scanner.nextDouble();
               cs1.saque(saque);
               System.out.println("Operação Finalizada! ");
               System.out.println("SALDO: R$ "+cs1.getSaldoSistema());
                break;
            }
        }
    }while (opcao!=0);

    

}
}

