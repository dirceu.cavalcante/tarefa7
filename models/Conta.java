package models;
public abstract class Conta {
    private int numero;
    private int agencia;
    private String banco;
    private String nome;
    private long cpf;
    protected double saldo;

    public Conta (int numero, int agencia, String banco, String nome, long cpf, double saldo){
        this.numero = numero;
        this.agencia = agencia;
        this.banco = banco;
        this.nome = nome;
        this.cpf = cpf;
        this.saldo = saldo;
    }

    public int getNumero(){
        return numero;
    }
    public void setNumero(int numero){
        this.numero = numero;
    }

    public int getAgencia(){
        return agencia;
    }
    public void setAgencia(int agencia){
        this.agencia = agencia;
    }

    public String getBanco(){
        return banco;
    }
    public void setBanco(String banco){
        this.banco = banco;
    }

    public String getNome(){
        return nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }

    public long getCPF(){
        return cpf;
    }
    public void setCPF(long cpf){
        this.cpf = cpf;
    }

    

}
