package models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ContaPoupanca extends Conta{
    public int aniversario;
    public boolean fezAniversário = false;
    public double taxa;
    public Date hoje = Calendar.getInstance().getTime();
    public SimpleDateFormat sdf = new SimpleDateFormat("dd");
   
    public void ajusteAniversario(){
        if (((aniversario <= Integer.parseInt(sdf.format(aniversario)))) && (!fezAniversário)){
            saldo = (1 + (taxa/100)) * saldo;
            setSaldo (arredonda(saldo));
            this.fezAniversário = true;
        }
    }

    public ContaPoupanca(int numero, int agencia, String banco, String nome, long cpf, 
    double saldo, double taxa, int aniversario){
        super(numero, agencia, banco, nome, cpf, saldo);
        this.taxa = taxa;
        this.aniversario = aniversario;
    }

    public double getTaxa(){
        return taxa;
    }

    public void setTaxa(double taxa){
        this.taxa = taxa;
    }
    
    public double getSaldo(){
        ajusteAniversario();
        return saldo;
    }

    

    public void setSaldo (double saldo){
        this.saldo = saldo;
    }

    public void deposito(double deposito){
        setSaldo(arredonda(getSaldo() + deposito));
        System.out.println("\n Depositando R$ " + deposito);
        System.out.println("Saldo atual R$ "+ getSaldo());
    }

    public void saque(double saque){
        double transacao = getSaldo() - saque;
        if (transacao < 0.0)
        {
            System.out.println("\nSaldo Insuficiente!!!");
        } else{
            System.out.println("Sacando R$ "+ saque + " ...");
            System.out.println("Saque Concluído!");
            setSaldo(arredonda(saldo - saque));
            System.out.println("Saldo Atual: " + getSaldo());
            
        }
    }
    
    public String toString(){
        return "Conta Corrente = {Agência " + getAgencia() +
        ", Número " + getNumero() + 
        ", " + getBanco() +
        ", " + getNome() +
        ", CPF " + getCPF() + 
        ", Taxa: " + getTaxa() +
        ", Aniversário da Conta: " + aniversario + "}";
    }

    public double arredonda(double valor){
        double d = Math.round(valor*100);
        return d/100;
    }
}
